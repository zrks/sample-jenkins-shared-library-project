@Grab('com.lesfurets.jenkins.unit.BasePipelineTest')
import com.lesfurets.jenkins.unit.BasePipelineTest

class TestJenkinsPipeline extends BasePipelineTest {
    
    @Test
    void should_execute_without_errors() throws Exception {
        def script = loadScript("../sample-jenkins-shared-library-project/Jenkinsfile")    
        script.execute()
        printCallStack()
    }

}
